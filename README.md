# JCenterProject

封装库

* 0.1.4 注释掉JZJsWebView#80 setDefaultHandler(new DefaultHandler());
* 0.1.3 JZJsWebView中对WebSettings修改，设置cache方式为default
* 0.1.1~0.1.2 JZDialog及JZAlertDialog解决"Can not perform this action after onSaveInstanceState"的问题
* 0.1.0 修改手机号正则
* 0.0.9 解决JZDropMenu点击外部不触发自定义监听的bug
* 0.0.7 升级X5WebView jar包，支持文档打开能力
* o.o.6 修改WebView内核为腾讯x5内核
* 0.0.5 更改Toast引用，修改EmptyView布局