package com.jackzhang.base.bean;

import android.support.annotation.DrawableRes;

public interface BarData
{
  String getTitle();

  @DrawableRes
  int getUnselectDrawable();

  @DrawableRes
  int getSelectDrawable();
}
