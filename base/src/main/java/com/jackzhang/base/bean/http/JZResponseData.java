package com.jackzhang.base.bean.http;

/**
 * 接口返回抽象实体类
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:42
 */
public abstract class JZResponseData<T>
{
  public abstract int getCode();

  public abstract T getData();

  public abstract String getMessage();
}
