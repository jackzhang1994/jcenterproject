package com.jackzhang.base.bean;

/**
 * 本地通讯录实体类
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:43
 */
public class LocalAddressBookBean
{
  /**
   * remarks : 你好
   * phone : 13691218979
   */

  private String remarks;
  private String phone;

  public String getRemarks()
  {
    return remarks;
  }

  public void setRemarks(String remarks)
  {
    this.remarks = remarks;
  }

  public String getPhone()
  {
    return phone;
  }

  public void setPhone(String phone)
  {
    this.phone = phone;
  }
}
