package com.jackzhang.base.bean.http;

/**
 * 接口异常实体类
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:42
 */
public class ApiException extends Exception
{
  private int code;

  public ApiException(String message, int code)
  {
    super(message);
    this.code = code;
  }

  public ApiException(String msg)
  {
    super(msg);
  }

  public int getCode()
  {
    return code;
  }

  public void setCode(int code)
  {
    this.code = code;
  }
}
