package com.jackzhang.base.base;

import android.content.Intent;
import android.support.annotation.StringRes;

import com.hjq.toast.ToastUtils;
import com.jackzhang.base.R;
import com.jackzhang.base.utils.KeyboardUtils;
import com.jackzhang.base.utils.NetworkUtils;
import com.jackzhang.base.widget.dialog.JZLoadingDialog;

import me.yokeyword.fragmentation.ISupportFragment;

/**
 * 基类MvpActivity
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:41
 */
public abstract class JZMvpActivity extends JZActivity implements JZMvpView
{
  @Override
  public void start(ISupportFragment toFragment)
  {
    hideKeyboard();
    super.start(toFragment);
  }

  @Override
  public void startActivity(Intent intent)
  {
    hideKeyboard();
    super.startActivity(intent);
  }

  @Override
  public void showLoading()
  {
    hideLoading();
    JZLoadingDialog.show(mContext);
  }

  @Override
  public void showLoading(int resId)
  {
    hideLoading();
    JZLoadingDialog.show(mContext, getString(resId));
  }

  @Override
  public void hideLoading()
  {
    JZLoadingDialog.dismiss(mContext);
  }

  @Override
  public void onError(@StringRes int resId)
  {
    showMessage(getString(resId));
  }

  @Override
  public void onError(String message)
  {
    if (message != null)
      showMessage(message);
    else
      showMessage(getString(R.string.jz_error_toast_error_tips));
  }

  @Override
  public void showMessage(@StringRes int resId)
  {
    showMessage(getString(resId));
  }

  @Override
  public void showMessage(String message)
  {
    if (message != null)
      ToastUtils.show(message);
    else
      ToastUtils.show(R.string.jz_error_toast_error_tips);
  }

  @Override
  public boolean isNetworkConnected()
  {
    return NetworkUtils.isNetworkConnected(this);
  }

  @Override
  public void hideKeyboard()
  {
    KeyboardUtils.hideSoftInput(this);
  }
}
