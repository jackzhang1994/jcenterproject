package com.jackzhang.base.base;

/**
 * 基类MvpPresenter
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:40
 */
public interface JZMvpPresenter<V extends JZMvpView>
{
  void onAttach(V mvpView);

  void onDetach();

  void setUserAsLoggedOut();

  /**
   * 判断登录状态
   */
  void checkLoginStatus();

  /**
   * 退出登录
   */
  void logout(boolean needFinish);
}
