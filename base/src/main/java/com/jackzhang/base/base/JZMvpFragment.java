package com.jackzhang.base.base;

import android.content.Intent;
import android.support.annotation.StringRes;

import com.jackzhang.base.utils.NetworkUtils;

import me.yokeyword.fragmentation.ISupportFragment;

/**
 * 基类MvpFragment
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:41
 */
public abstract class JZMvpFragment extends JZFragment implements JZMvpView
{
  @Override
  public void onDestroyView()
  {
    hideKeyboard();
    super.onDestroyView();
  }

  @Override
  public void showLoading()
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).showLoading();
  }

  @Override
  public void showLoading(int resId)
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).showLoading(resId);
  }

  @Override
  public void hideLoading()
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).hideLoading();
  }

  @Override
  public void onError(@StringRes int resId)
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).onError(resId);
  }

  @Override
  public void onError(String message)
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).onError(message);
  }

  @Override
  public void showMessage(String message)
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).showMessage(message);
  }

  @Override
  public void showMessage(@StringRes int resId)
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).showMessage(resId);
  }

  @Override
  public boolean isNetworkConnected()
  {
    return NetworkUtils.isNetworkConnected(mContext);
  }

  @Override
  public void hideKeyboard()
  {
    if (getActivity() == null) return;
    ((JZMvpActivity) getActivity()).hideKeyboard();
  }

  @Override
  public void start(ISupportFragment toFragment)
  {
    hideKeyboard();
    super.start(toFragment);
  }

  @Override
  public void startActivity(Intent intent)
  {
    hideKeyboard();
    super.startActivity(intent);
  }
}
