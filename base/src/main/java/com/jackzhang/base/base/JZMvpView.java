package com.jackzhang.base.base;

import android.support.annotation.StringRes;

/**
 * 基类MvpView
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:26
 */
public interface JZMvpView
{
  /**
   * 显示Loading
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:27
   */
  void showLoading();

  void showLoading(@StringRes int resId);

  /**
   * 隐藏Loading
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:27
   */
  void hideLoading();

  /**
   * 错误信息提示
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:27
   */
  void onError(@StringRes int resId);

  void onError(String message);

  /**
   * 显示吐司
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:27
   */
  void showMessage(String message);

  void showMessage(@StringRes int resId);

  /**
   * 网络是否连接
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:27
   */
  boolean isNetworkConnected();

  /**
   * 隐藏软键盘
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:28
   */
  void hideKeyboard();

  /**
   * 是否已经登录
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:28
   */
  void isLogin(boolean isLogin);

  /**
   * 退出登录成功回调
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:28
   */
  void logoutSuccess();

  /**
   * 强制登出方法
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:49
   */
  void mustLogout();

  /**
   * 强制登出code码
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:49
   */
  int mustLogoutCode();
}
