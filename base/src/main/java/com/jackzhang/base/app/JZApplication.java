package com.jackzhang.base.app;

import android.app.Activity;
import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.hjq.toast.ToastUtils;
import com.jackzhang.base.utils.CrashUtils;
import com.jackzhang.base.utils.SystemUtils;
import com.jackzhang.base.widget.webView.JZWebViewUtils;
import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Description: 基类Application
 * Author: Jack Zhang
 * create on: 2019-04-28 19:17
 */
public abstract class JZApplication extends MultiDexApplication
{
  private static List<Activity> mActivitys;

  public static synchronized JZApplication get(Context context)
  {
    return (JZApplication) context.getApplicationContext();
  }

  @Override
  public void onCreate()
  {
    super.onCreate();
    SystemUtils.closeAndroidPDialog();

    // 初始化Toast
    ToastUtils.init(this);

    configGlobalInit();
  }



  /**
   * 配置APP全局初始化环境
   */
  public abstract void configGlobalInit();

  /**
   * 配置开发环境工具
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:19
   */
  public void configDevelopment()
  {
    CrashUtils.initCrashLog(this);
  }

  /**
   * 添加activity
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:20
   */
  public void addActivity(Activity activity)
  {
    if (mActivitys == null)
      mActivitys = new ArrayList<>();
    mActivitys.add(activity);
  }

  /**
   * 移除activity
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:20
   */
  public void removeActivity(Activity activity)
  {
    if (mActivitys != null)
      mActivitys.remove(activity);
  }

  /**
   * 退出应用
   *
   * @author Jack Zhang
   * create at 2019-04-28 19:20
   */
  public void exitApp()
  {
    if (mActivitys != null)
    {
      synchronized (JZApplication.class)
      {
        for (Activity mActivity : mActivitys)
          mActivity.finish();
      }
    }
    //结束进程
    android.os.Process.killProcess(android.os.Process.myPid());
    System.exit(0);
  }
}
