package com.jackzhang.base.utils.permission;

/**
 * 权限监听
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:54
 */
public interface PermissionListener
{
  void onGrantedPermission(@Permission.PermissionType int permissionType);
}
