package com.jackzhang.base.utils;

import android.text.TextUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * String工具类
 *
 * @author Jack Zhang
 * create at 2019-04-29 00:13
 */
public class StringUtils
{
  /**
   * 字符串MD5
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:13
   */
  public static String md5(String string)
  {
    if (TextUtils.isEmpty(string))
    {
      return "";
    }
    try
    {
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      byte[] bytes = md5.digest(string.getBytes());
      StringBuilder result = new StringBuilder();
      for (byte b : bytes)
      {
        String temp = Integer.toHexString(b & 0xff);
        if (temp.length() == 1)
          temp = "0" + temp;
        result.append(temp);
      }
      return result.toString();
    } catch (NoSuchAlgorithmException e)
    {
      e.printStackTrace();
    }
    return string;
  }

  /**
   * 根据字符把list转成字符串
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:13
   */
  public static String list2String(List<String> list, char separator)
  {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < list.size(); i++)
    {
      sb.append(list.get(i));
      if (i < list.size() - 1)
        sb.append(separator);
    }
    return sb.toString();
  }
}
