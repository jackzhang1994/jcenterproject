package com.jackzhang.base.utils;

import android.app.ActivityManager;
import android.content.Context;

/**
 * 进程工具类
 *
 * @author Jack Zhang
 * create at 2019-04-29 00:12
 */
public class ProcessUtils
{
  /**
   * 包名判断是否为主进程
   */
  public static boolean isMainProcess(Context context)
  {
    return context.getPackageName().equals(getCurProcessName(context));
  }

  private static String getCurProcessName(Context context)
  {
    int pid = android.os.Process.myPid();
    ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses())
      if (appProcess.pid == pid)
        return appProcess.processName;
    return null;
  }
}
