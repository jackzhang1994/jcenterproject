package com.jackzhang.base.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 软键盘工具类
 *
 * @author Jack Zhang
 * create at 2019-04-28 20:02
 */
public final class KeyboardUtils
{
  /**
   * 隐藏软键盘
   *
   * @author Jack Zhang
   * create at 2019-04-28 20:03
   */
  public static void hideSoftInput(Activity activity)
  {
    View view = activity.getCurrentFocus();
    if (view == null) view = new View(activity);
    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
    if (imm != null)
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
  }

  /**
   * 隐藏软键盘
   *
   * @author Jack Zhang
   * create at 2019-04-28 20:03
   */
  public static void hideSoftInput(EditText editText, Context context)
  {
    InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
    if (imm != null)
      imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
  }

  /**
   * 显示软键盘 (不推荐使用)
   *
   * @author Jack Zhang
   * create at 2019-04-28 20:03
   */
  public static void showSoftInput(EditText edit, Context context)
  {
    edit.setFocusable(true);
    edit.setFocusableInTouchMode(true);
    edit.requestFocus();
    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    if (imm != null)
      imm.showSoftInput(edit, 0);
  }

  /**
   * 软键盘是否显示
   *
   * @author Chenlei
   * created at 2018/9/14
   **/
  public static boolean isKeyboardShow(Context context)
  {
    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    return imm != null && imm.isActive();
  }

  /**
   * 切换软键盘显示状态
   *
   * @author Jack Zhang
   * create at 2019-04-28 20:04
   */
  public static void toggleSoftInput(Context context)
  {
    // 到InputMethodManager的实例
    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    // 关闭软键盘，开启方法相同，这个方法是切换开启与关闭状态的
    if (imm != null)
      imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
  }
}
