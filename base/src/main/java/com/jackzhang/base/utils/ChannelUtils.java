package com.jackzhang.base.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

/**
 * Channel工具类
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:54
 */
public class ChannelUtils
{
  public static String getChannelValue(Context context)
  {
    String value = "JZ";
    String channelNameKey = "CHANNEL_NAME";
    try
    {
      ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
      value = appInfo.metaData.getString(channelNameKey);
    } catch (PackageManager.NameNotFoundException e)
    {
      e.printStackTrace();
    }
    return value;
  }
}
