package com.jackzhang.base.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * UI工具类——单位转换
 *
 * @author Jack Zhang
 * create at 2019-04-29 00:15
 */
public class UiUtils
{
  /**
   * dp - px
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:16
   */
  public static int dp2px(Context context, float dpValue)
  {
    final float scale = context.getResources().getDisplayMetrics().density;
    return (int) (dpValue * scale + 0.5f);
  }

  /**
   * px - dp
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:16
   */
  public static int px2dp(Context context, float pxValue)
  {
    final float scale = context.getResources().getDisplayMetrics().density;
    return (int) (pxValue / scale + 0.5f);
  }

  /**
   * sp - px
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:16
   */
  public static int sp2px(Context context, float spVal)
  {
    return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spVal, context.getResources().getDisplayMetrics());
  }

  /**
   * px - sp
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:16
   */
  public static int px2sp(Context context, float pxValue)
  {
    final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
    return (int) (pxValue / fontScale + 0.5f);
  }

  /**
   * 获取屏幕高度
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:16
   */
  public static int windowHeight(Context context)
  {
    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
    return metrics.heightPixels;
  }

  /**
   * 获取屏幕宽度
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:17
   */
  public static int windowWidth(Context context)
  {
    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
    return metrics.widthPixels;
  }

  /**
   * 获取状态栏高度
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:17
   */
  @SuppressLint("PrivateApi")
  public static int statusBarHeight(Context context)
  {
    int statusBarHeight = -1;
    try
    {
      Class<?> clazz = Class.forName("com.android.internal.R$dimen");
      Object object = clazz.newInstance();
      int height = Integer.parseInt(clazz.getField("status_bar_height")
              .get(object).toString());
      statusBarHeight = context.getResources().getDimensionPixelSize(height);
    } catch (Exception e)
    {
      e.printStackTrace();
    }
    return statusBarHeight;
  }

  /**
   * 获取View的真实宽高
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:17
   */
  public static void measureWidthAndHeight(View view)
  {
    int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
    int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
    view.measure(widthMeasureSpec, heightMeasureSpec);
  }

  /**
   * 动态设置View的高度
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:17
   */
  public static void setAutoHeight(View view, int width, float ratio)
  {
    int height = (int) (width / ratio);
    ViewGroup.LayoutParams params = view.getLayoutParams();
    params.height = height;
    view.setLayoutParams(params);
  }

  /**
   * 下移状态栏高度  用于适配一个界面侵入式样式并存
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:18
   */
  public static void moveStatusBarHeight(View view)
  {
    setMargins(view, 0, statusBarHeight(view.getContext()), 0, 0);
  }

  /**
   * 设置间距
   *
   * @author Jack Zhang
   * create at 2019-04-29 00:18
   */
  public static void setMargins(View view, int left, int top, int right, int bottom)
  {
    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
    params.setMargins(left, top, right, bottom);
    view.setLayoutParams(params);
  }
}
