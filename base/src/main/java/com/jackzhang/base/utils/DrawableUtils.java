package com.jackzhang.base.utils;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;

/**
 * Drawable工具类
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:57
 */
public class DrawableUtils
{
  /**
   * @param drawableNormal 默认
   * @param drawableSelect 选中
   * @return StateListDrawable
   */
  public static StateListDrawable getSelectorDrawable(Drawable drawableNormal, Drawable drawableSelect)
  {
    StateListDrawable drawable = new StateListDrawable();
    //选中
    drawable.addState(new int[]{android.R.attr.state_selected}, drawableSelect);
    //未选中
    drawable.addState(new int[]{-android.R.attr.state_selected}, drawableNormal);
    return drawable;
  }
}
