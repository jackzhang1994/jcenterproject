package com.jackzhang.base.utils.glide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.jackzhang.base.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Glide工具类
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:51
 */
public class GlideUtils
{
  public static final int CENTER_CROP = 0;
  public static final int CENTER_INSIDE = 1;
  public static final int FIT_CENTER = 2;

  @IntDef({CENTER_CROP, CENTER_INSIDE, FIT_CENTER})
  @Retention(RetentionPolicy.SOURCE)
  public @interface ScaleType
  {
  }

  public static void showImage(Context context, Object path, ImageView imageView)
  {
    showImage(context, path, imageView, CENTER_CROP);
  }

  public static void showImage(Context context, Object path, ImageView imageView, @ScaleType int scaleType)
  {
    showImage(context, path, imageView, 0, scaleType);
  }

  public static void showImageAndError(Context context, Object path, ImageView imageView, int errorId)
  {
    showImage(context, path, imageView, errorId, CENTER_CROP);
  }

  /**
   * 显示图片
   *
   * @author Chenlei
   * created at 2018/9/19
   **/
  public static void showImage(Context context, Object path, ImageView imageView, int errorId, @ScaleType int scaleType)
  {
    if (errorId == 0)
      errorId = R.drawable.icon_image_empty;
    RequestOptions options;
    if (path instanceof Integer)
      options = GlideRequestOptionsUtils.getOptions(null);
    else
      options = GlideRequestOptionsUtils.getOptions(ContextCompat.getDrawable(context, errorId));
    switch (scaleType)
    {
      case CENTER_CROP:
        options.centerCrop();
        break;
      case CENTER_INSIDE:
        options.centerInside();
        break;
      case FIT_CENTER:
        options.fitCenter();
        break;
    }
    Glide.with(context)
            .load(path)
            .apply(options)
            .into(imageView);
  }

  /**
   * 加载图片，带有监听
   *
   * @author Jack Zhang
   * create at 2018/12/12 3:35 PM
   */
  public static void showImageWithListener(Context context, Object path, ImageView imageView, int errorId, loadImgListener listener)
  {
    if (errorId == 0)
      errorId = R.drawable.icon_image_empty;
    RequestOptions options;
    if (path instanceof Integer)
      options = GlideRequestOptionsUtils.getOptions(null).centerCrop();
    else
      options = GlideRequestOptionsUtils.getOptions(ContextCompat.getDrawable(context, errorId)).centerCrop();
    Glide.with(context)
            .load(path)
            .apply(options)
            .listener(new RequestListener<Drawable>()
            {
              @Override
              public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource)
              {
                if (listener != null)
                  listener.onLoadFailed();
                return false;
              }

              @Override
              public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource)
              {
                if (listener != null)
                  listener.onLoadSuccess();
                return false;
              }
            })
            .into(imageView);
  }

  public interface loadImgListener
  {
    void onLoadFailed();

    void onLoadSuccess();
  }
}
