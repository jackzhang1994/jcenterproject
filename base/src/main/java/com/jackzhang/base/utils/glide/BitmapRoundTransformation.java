package com.jackzhang.base.utils.glide;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

/**
 * Glide圆角工具类
 *
 * @author Jack Zhang
 * create at 2018/11/22 11:02 AM
 */
public class BitmapRoundTransformation extends BitmapTransformation
{
  private float radius = 0f;

  public BitmapRoundTransformation()
  {
    this(4);
  }

  public BitmapRoundTransformation(int radius)
  {
    super();
    this.radius = radius;
  }

  @Override
  protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight)
  {
    return roundCrop(pool, toTransform);
  }

  private Bitmap roundCrop(BitmapPool pool, Bitmap source)
  {
    if (source == null) return null;

    Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);

    Canvas canvas = new Canvas(result);
    Paint paint = new Paint();
    paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
    paint.setAntiAlias(true);
    RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
    canvas.drawRoundRect(rectF, radius, radius, paint);
    return result;
  }

  @Override
  public void updateDiskCacheKey(@NonNull MessageDigest messageDigest)
  {

  }
}