package com.jackzhang.base.rxjava;

import io.reactivex.subscribers.ResourceSubscriber;

/**
 * 自定义抽象的FlowableSubscriber
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:48
 */
public abstract class JZFlowableSubscriber<T> extends ResourceSubscriber<T>
{
  @Override
  public void onNext(T t)
  {

  }

  @Override
  public void onError(Throwable t)
  {

  }

  @Override
  public void onComplete()
  {

  }
}
