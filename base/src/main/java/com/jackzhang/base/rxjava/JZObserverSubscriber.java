package com.jackzhang.base.rxjava;

import android.text.TextUtils;

import com.google.gson.JsonSyntaxException;

import java.net.SocketException;
import java.net.UnknownHostException;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.ResourceObserver;

import com.jackzhang.base.R;
import com.jackzhang.base.base.JZMvpView;
import com.jackzhang.base.bean.http.ApiException;

import retrofit2.HttpException;

/**
 * 自定义抽象的ObserverSubscriber
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:48
 */
public abstract class JZObserverSubscriber<T> extends ResourceObserver<T>
{
  private JZMvpView mJZMvpView;
  private String mErrorMsg;

  public JZObserverSubscriber(JZMvpView JZMvpView)
  {
    mJZMvpView = JZMvpView;
  }

  public JZObserverSubscriber(JZMvpView JZMvpView, String errorMsg)
  {
    mJZMvpView = JZMvpView;
    mErrorMsg = errorMsg;
  }

  @Override
  public void onNext(@NonNull T t)
  {
    if (mJZMvpView == null) return;
    mJZMvpView.hideLoading();
  }

  @Override
  public void onError(@NonNull Throwable e)
  {
    if (mJZMvpView == null) return;

    mJZMvpView.hideLoading();

    if (mErrorMsg != null && !TextUtils.isEmpty(mErrorMsg))
      // 请求数据正确连接后返回的异常信息
      mJZMvpView.onError(mErrorMsg);
    else if (e instanceof ApiException)
    {
      mJZMvpView.onError(e.getMessage());
      if (((ApiException) e).getCode() == mJZMvpView.mustLogoutCode())
        mJZMvpView.mustLogout();
    } else if (e instanceof JsonSyntaxException)
      mJZMvpView.onError(R.string.jz_api_data_analysis_failed_error);
    else if (e instanceof HttpException)
      mJZMvpView.onError(R.string.jz_api_data_load_failed_error);
    else if (e instanceof UnknownHostException || e instanceof SocketException)
      mJZMvpView.onError(R.string.jz_api_network_error);
    else
      mJZMvpView.onError(R.string.jz_api_server_connect_error);
  }

  @Override
  public void onComplete()
  {

  }
}
