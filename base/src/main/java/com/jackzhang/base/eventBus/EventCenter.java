package com.jackzhang.base.eventBus;

/**
 * EventBus订阅中心
 *
 * @author Jack Zhang
 * create at 2019-04-28 19:43
 */
public class EventCenter<T>
{
  /**
   * 事件携带的数据
   */
  private T data;

  /**
   * 区分不同的事件
   */
  private int eventCode = -1;

  public EventCenter(T data, int eventCode)
  {
    this.data = data;
    this.eventCode = eventCode;
  }

  public EventCenter(int eventCode)
  {
    this(null, eventCode);
  }

  public T getData()
  {
    return data;
  }

  public int getEventCode()
  {
    return eventCode;
  }
}
