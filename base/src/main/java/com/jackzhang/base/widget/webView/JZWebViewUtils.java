package com.jackzhang.base.widget.webView;

import android.content.Context;
import android.util.Log;

import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;

import java.util.HashMap;

/**
 * Description: WebView初始化工具类
 * Author: Jack Zhang
 * create on: 2019-05-13 13:49
 */
public class JZWebViewUtils
{
  /**
   * 异步初始化腾讯X5内核，传入ApplicationContext对象
   *
   * @author Jack Zhang
   * create at 2019-05-13 13:51
   */
  public static void initQbSdk(Context applicationContext)
  {
    HashMap<String, Object> map = new HashMap<>();
    map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
    QbSdk.initTbsSettings(map);

    // 会导致内核初始化失败
//    if (!QbSdk.isTbsCoreInited())
//      QbSdk.preInit(applicationContext);// 设置X5初始化完成的回调接口

    //搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。
    QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback()
    {
      @Override
      public void onViewInitFinished(boolean arg0)
      {
        // x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
      }

      @Override
      public void onCoreInitFinished()
      {
      }
    };

    //x5内核初始化接口
    QbSdk.initX5Environment(applicationContext, cb);
  }
}
