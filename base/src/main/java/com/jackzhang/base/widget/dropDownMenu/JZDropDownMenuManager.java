package com.jackzhang.base.widget.dropDownMenu;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * 设置不可滑动的RecyclerView Manager
 *
 * @author Jack Zhang
 * create at 2019-04-29 00:36
 */
public class JZDropDownMenuManager extends LinearLayoutManager
{
  private boolean isScrollEnabled = true;

  public JZDropDownMenuManager(Context context)
  {
    super(context);
  }

  public void setScrollEnabled(boolean flag)
  {
    this.isScrollEnabled = flag;
  }

  @Override
  public boolean canScrollVertically()
  {
    //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
    return isScrollEnabled && super.canScrollVertically();
  }
}
