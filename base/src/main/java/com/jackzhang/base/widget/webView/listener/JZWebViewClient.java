package com.jackzhang.base.widget.webView.listener;

import android.graphics.Bitmap;

import com.jackzhang.base.widget.webView.jsBridge.BridgeWebView;
import com.jackzhang.base.widget.webView.jsBridge.BridgeWebViewClient;
import com.tencent.smtt.sdk.WebView;

public class JZWebViewClient extends BridgeWebViewClient
{
  private JZWebListener.WebClientListener mClientListener;

  public JZWebViewClient(BridgeWebView webView)
  {
    this(webView, null);
  }

  public JZWebViewClient(BridgeWebView webView, JZWebListener.WebClientListener clientListener)
  {
    super(webView);
    this.mClientListener = clientListener;
  }

  @Override
  public boolean shouldOverrideUrlLoading(WebView view, String url)
  {
    if (mClientListener != null)
      return mClientListener.shouldOverrideUrlLoading(view, url);
    else
      return super.shouldOverrideUrlLoading(view, url);
  }

  @Override
  public void onPageStarted(WebView view, String url, Bitmap favicon)
  {
    super.onPageStarted(view, url, favicon);
    if (mClientListener != null)
      mClientListener.onPageStarted(url);
  }

  @Override
  public void onPageFinished(WebView view, String url)
  {
    super.onPageFinished(view, url);
    if (mClientListener != null)
      mClientListener.onPageFinished(view, url);
  }

  @Override
  public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
  {
    super.onReceivedError(view, errorCode, description, failingUrl);
    if (mClientListener != null)
      mClientListener.onReceivedError(view, errorCode, description, failingUrl);
  }
}
