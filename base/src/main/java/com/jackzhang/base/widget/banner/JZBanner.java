package com.jackzhang.base.widget.banner;

import android.content.Context;
import android.util.AttributeSet;

import com.youth.banner.Banner;

public class JZBanner extends Banner
{
  private Context mContext;

  public JZBanner(Context context)
  {
    this(context, null);
  }

  public JZBanner(Context context, AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public JZBanner(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    initBanner(context);
  }

  private void initBanner(Context context)
  {
    mContext = context;
    setImageLoader(new JZBannerImageLoader());
  }
}
