package com.jackzhang.base.widget.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.JZ.widget.adapter.viewHolder.JZViewHolder;
import com.jackzhang.base.R;

import java.util.List;

public abstract class JZMultiItemQuickAdapter<T extends MultiItemEntity, K extends JZViewHolder> extends BaseMultiItemQuickAdapter<T, JZViewHolder>
{
  public JZMultiItemQuickAdapter(List<T> data)
  {
    super(data);
  }

  @Override
  protected View getItemView(int layoutResId, ViewGroup parent)
  {
    ViewDataBinding binding = DataBindingUtil.inflate(mLayoutInflater, layoutResId, parent, false);
    if (binding == null)
    {
      return super.getItemView(layoutResId, parent);
    }
    View view = binding.getRoot();
    view.setTag(R.id.BaseQuickAdapter_databinding_support, binding);
    return view;
  }
}
