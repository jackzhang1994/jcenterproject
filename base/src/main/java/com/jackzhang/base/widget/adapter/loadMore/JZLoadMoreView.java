package com.jackzhang.base.widget.adapter.loadMore;

import com.chad.library.adapter.base.loadmore.LoadMoreView;
import com.jackzhang.base.R;

/**
 * 自定义LoadMoreView样式
 *
 * @author Jack Zhang
 * create at 2019-04-29 00:45
 */
public final class JZLoadMoreView extends LoadMoreView
{
  @Override
  public int getLayoutId()
  {
    return R.layout.jz_load_more_layout;
  }

  @Override
  protected int getLoadingViewId()
  {
    return R.id.load_more_loading_view;
  }

  @Override
  protected int getLoadFailViewId()
  {
    return R.id.load_more_load_fail_view;
  }

  @Override
  protected int getLoadEndViewId()
  {
    return R.id.load_more_load_end_view;
  }
}
