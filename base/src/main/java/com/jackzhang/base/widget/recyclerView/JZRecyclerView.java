package com.jackzhang.base.widget.recyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jackzhang.base.utils.SmartRefreshUtils;
import com.jackzhang.base.widget.adapter.emptyView.JZEmptyView;
import com.jackzhang.base.widget.adapter.loadMore.JZLoadMoreView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

public abstract class JZRecyclerView<T> extends RecyclerView
{
  protected Context mContext;//上下文
  private BaseQuickAdapter<T, BaseViewHolder> mAdapter;//标准adapter
  private SmartRefreshLayout mRefreshLayout;//标准下拉刷新
  private JZEmptyView mEmptyView;//空布局
  private JZEmptyView.OnClickListener mOnClickListener;

  public JZRecyclerView(@NonNull Context context)
  {
    this(context, null);
  }

  public JZRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public JZRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    initRecyclerView(context);
  }

  /**
   * 初始化recyclerview
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:04
   */
  private void initRecyclerView(Context context)
  {
    mContext = context;
    mAdapter = initAdapter();
    setLayoutManager(initLayoutManager());
    if (getLayoutManager() instanceof GridLayoutManager)
    {
      mAdapter.setHeaderViewAsFlow(false);
      mAdapter.setFooterViewAsFlow(false);
    }
    mAdapter.bindToRecyclerView(this);
  }

  /**
   * 是否嵌套使用recyclerview
   * 解决卡顿
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:05
   */
  public JZRecyclerView setNested(boolean isNested)
  {
    setHasFixedSize(!isNested);
    setNestedScrollingEnabled(!isNested);
    return this;
  }

  /**
   * 开启空布局
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:05
   */
  public JZRecyclerView openEmptyView(JZEmptyView.OnClickListener onClickListener)
  {
    mOnClickListener = onClickListener;
    mAdapter.setEmptyView(getEmptyView());
    getEmptyView().setOnClickListener((view, state) ->
    {
      if (state == JZEmptyView.STATE_ERROR)
        settingEmptyView();
      if (mOnClickListener != null)
        mOnClickListener.onClick(view, state);
    });
    settingEmptyView();
    return this;
  }

  /**
   * 首次加载失败
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:05
   */
  public JZRecyclerView firstLoadFailed()
  {
    finishRefresh();
    if (mAdapter.getEmptyView() != null)
    {
      mAdapter.setHeaderAndEmpty(false);
      mAdapter.setNewData(new ArrayList<>());
      getEmptyView().setState(JZEmptyView.STATE_ERROR);
      if (mAdapter.getEmptyView().getVisibility() == GONE)
        mAdapter.getEmptyView().setVisibility(VISIBLE);
    }
    return this;
  }

  /**
   * 添加头布局
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:05
   */
  public JZRecyclerView addHeaderView(View headerView)
  {
    return addHeaderView(headerView, 0);
  }

  public JZRecyclerView addHeaderView(View headerView, int index)
  {
    mAdapter.setHeaderView(headerView, index);
    return this;
  }

  /**
   * 开启加载更多
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:05
   */
  public JZRecyclerView setOnLoadMoreListener(BaseQuickAdapter.RequestLoadMoreListener requestLoadMoreListener)
  {
    mAdapter.setLoadMoreView(new JZLoadMoreView());
    mAdapter.setOnLoadMoreListener(requestLoadMoreListener, this);
    return this;
  }

  /**
   * 加载更多失败
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:05
   */
  public JZRecyclerView loadMoreFailed()
  {
    mAdapter.loadMoreFail();
    return this;
  }

  /**
   * 设置数据
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:06
   */
  public JZRecyclerView setData(List<T> data)
  {
    finishRefresh();
    if (mAdapter.getEmptyView() != null)
    {
      if (mAdapter.getEmptyView().getVisibility() == GONE)
        mAdapter.getEmptyView().setVisibility(VISIBLE);
      getEmptyView().setState(JZEmptyView.STATE_NORMAL);
    }
    mAdapter.setHeaderAndEmpty(true);
    mAdapter.setNewData(data);
    if (data != null && data.size() < getLoadNum())
      mAdapter.disableLoadMoreIfNotFullPage();
    return this;
  }

  /**
   * 加载更多数据
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:06
   */
  public JZRecyclerView loadData(List<T> data)
  {
    if (mRefreshLayout != null)
      mRefreshLayout.setEnableRefresh(true);
    mAdapter.addData(data);
    if (data.size() < getLoadNum())
      mAdapter.loadMoreEnd();
    else
      mAdapter.loadMoreComplete();
    return this;
  }

  /**
   * 获取数据源
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:06
   */
  public List<T> getData()
  {
    return mAdapter.getData();
  }

  /**
   * 获取adapter
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:06
   */
  public BaseQuickAdapter getAdapter()
  {
    return mAdapter;
  }

  /**
   * 设置下拉刷新  自定义样式后传入
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:07
   */
  public JZRecyclerView setRefreshLayout(SmartRefreshLayout refreshLayout)
  {
    mRefreshLayout = refreshLayout;
    return this;
  }

  /**
   * 通用下拉  无样式
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:07
   */
  public JZRecyclerView setRefreshLayout(SmartRefreshLayout refreshLayout, OnRefreshListener onRefreshListener)
  {
    SmartRefreshUtils.initRefresh(mContext, refreshLayout, onRefreshListener);
    return setRefreshLayout(refreshLayout);
  }

  /**
   * 初始化adapter
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:07
   */
  public abstract BaseQuickAdapter initAdapter();

  /**
   * 初始化布局管理
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:07
   */
  public abstract RecyclerView.LayoutManager initLayoutManager();

  /**
   * 初始化空布局
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:07
   */
  public JZEmptyView getEmptyView()
  {
    if (mEmptyView == null)
    {
      mEmptyView = new JZEmptyView(mContext);
      configEmptyView(mEmptyView);
    }
    return mEmptyView;
  }

  /**
   * 配置空布局信息
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:07
   */
  protected void configEmptyView(JZEmptyView emptyView)
  {

  }

  /**
   * recyclerView滑动速度
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:07
   */
  @Override
  public boolean fling(int velocityX, int velocityY)
  {
    double ratio;
    if (getFlingRatio() > 1)
      ratio = 1;
    else if (getFlingRatio() < 0.1)
      ratio = 0.1;
    else
      ratio = getFlingRatio();
    velocityX *= ratio;
    velocityY *= ratio;
    return super.fling(velocityX, velocityY);
  }

  /**
   * 配置滑动recyclerView的速度 0.1~1之间
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:08
   */
  public double getFlingRatio()
  {
    return 1.0;
  }

  /**
   * 是否显示首次加载界面
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:08
   */
  public abstract boolean isFirst();

  /**
   * 一次加载数量
   *
   * @author Jack Zhang
   * create at 2019-04-29 01:08
   */
  public abstract int getLoadNum();

  private void finishRefresh()
  {
    if (mRefreshLayout != null && mRefreshLayout.isRefreshing())
      mRefreshLayout.finishRefresh();
  }

  private void settingEmptyView()
  {
    if (isFirst())
      getEmptyView().setState(JZEmptyView.STATE_LOADING);
    else
    {
      getEmptyView().setState(JZEmptyView.STATE_NORMAL);
      mAdapter.getEmptyView().setVisibility(GONE);
    }
  }
}
