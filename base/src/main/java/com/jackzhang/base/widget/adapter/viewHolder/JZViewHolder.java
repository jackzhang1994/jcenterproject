package com.JZ.widget.adapter.viewHolder;

import android.databinding.ViewDataBinding;
import android.view.View;

import com.chad.library.adapter.base.BaseViewHolder;
import com.jackzhang.base.R;

public class JZViewHolder extends BaseViewHolder
{
  public JZViewHolder(View view)
  {
    super(view);
  }

  public ViewDataBinding getBinding()
  {
    return (ViewDataBinding) itemView.getTag(R.id.BaseQuickAdapter_databinding_support);
  }
}
