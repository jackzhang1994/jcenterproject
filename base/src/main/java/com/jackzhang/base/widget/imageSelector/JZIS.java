package com.jackzhang.base.widget.imageSelector;

import android.graphics.Color;

import com.jackzhang.base.R;
import com.yuyh.library.imgsel.ISNav;
import com.yuyh.library.imgsel.config.ISCameraConfig;
import com.yuyh.library.imgsel.config.ISListConfig;

public class JZIS
{
  private static JZIS instance;

  public static JZIS getInstance()
  {
    if (instance == null)
      synchronized (ISNav.class)
      {
        if (instance == null)
          instance = new JZIS();
      }
    return instance;
  }

  private JZIS()
  {
    ISNav.getInstance().init(new JZISLoader());
  }

  public void toListActivity(Object source, int num, int reqCode)
  {
    ISListConfig.Builder builder = new ISListConfig.Builder().backResId(R.drawable.icon_back).statusBarColor(Color.WHITE).titleBgColor(Color.WHITE)
            .titleColor(Color.BLACK).btnTextColor(Color.BLACK);
    if (num > 1)
    {
      builder.maxNum(num);
      builder.multiSelect(true);
    } else
      builder.multiSelect(false);
    toListActivity(source, builder.build(), reqCode);
  }

  public void toListActivity(Object source, ISListConfig config, int reqCode)
  {
    ISNav.getInstance().toListActivity(source, config, reqCode);
  }

  public void toCameraActivity(Object source, ISCameraConfig config, int reqCode)
  {
    ISNav.getInstance().toCameraActivity(source, config, reqCode);
  }
}
