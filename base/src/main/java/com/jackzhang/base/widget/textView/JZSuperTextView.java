package com.jackzhang.base.widget.textView;

import android.content.Context;
import android.util.AttributeSet;

import com.allen.library.SuperTextView;

public class JZSuperTextView extends SuperTextView
{
  public JZSuperTextView(Context context)
  {
    super(context);
  }

  public JZSuperTextView(Context context, AttributeSet attrs)
  {
    super(context, attrs);
  }

  public JZSuperTextView(Context context, AttributeSet attrs, int defStyleAttr)
  {
    super(context, attrs, defStyleAttr);
  }
}
