package com.jackzhang.base.widget.webView.listener;

import android.net.Uri;

import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.JsPromptResult;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

public class JZWebListener
{
  public interface WebChromeClientListener
  {
    void onProgressChanged(WebView view, int newProgress);

    void onReceivedTitle(WebView view, String string);

    void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissionsCallback callback);

    boolean onJsAlert(WebView webView, String s, String s1, JsResult jsResult);

    boolean onJsPrompt(WebView webView, String s, String s1, String s2, JsPromptResult jsPromptResult);

    boolean onJsConfirm(WebView webView, String s, String s1, JsResult jsResult);

    boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams);

    void openFileChooser(ValueCallback<Uri> valueCallback, String s, String s1);
  }

  public interface WebClientListener
  {
    boolean shouldOverrideUrlLoading(WebView view, String url);

    void onPageStarted(String url);

    void onPageFinished(WebView view, String url);

    void onReceivedError(WebView view, int errorCode, String description, String failingUrl);
  }

  /**
   * 功能页面中使用，包含以上所有方法
   *
   * @author Jack Zhang
   * create at 2019-05-13 11:10
   */
  public interface OnLoadListener
  {
    boolean shouldOverrideUrlLoading(WebView view, String url);

    void onPageStarted(String url);

    void onPageFinished(WebView view, String url);

    void onReceivedTitle(String title);

    void onReceivedError(WebView view, int errorCode, String description, String failingUrl);

    void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissionsCallback callback);

    boolean onJsAlert(WebView webView, String s, String s1, JsResult jsResult);

    boolean onJsPrompt(WebView webView, String s, String s1, String s2, JsPromptResult jsPromptResult);

    boolean onJsConfirm(WebView webView, String s, String s1, JsResult jsResult);

    boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams);

    void openFileChooser(ValueCallback<Uri> valueCallback, String s, String s1);
  }
}
