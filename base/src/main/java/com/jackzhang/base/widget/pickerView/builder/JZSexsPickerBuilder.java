package com.jackzhang.base.widget.pickerView.builder;

import android.content.Context;

import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.jackzhang.base.R;
import com.jackzhang.base.widget.pickerView.view.JZSexsPickerView;

public class JZSexsPickerBuilder extends JZOptionsPickerBuilder
{
  public JZSexsPickerBuilder(Context context, OnOptionsSelectListener listener)
  {
    super(context, listener);
    setTitleText(context.getString(R.string.jz_picker_view_select_sexs));
  }

  public JZSexsPickerView build()
  {
    return new JZSexsPickerView(mPickerOptions);
  }
}
