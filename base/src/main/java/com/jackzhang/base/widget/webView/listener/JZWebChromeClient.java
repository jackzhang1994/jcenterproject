package com.jackzhang.base.widget.webView.listener;

import android.net.Uri;

import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.JsPromptResult;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

public class JZWebChromeClient extends WebChromeClient
{
  private JZWebListener.WebChromeClientListener mWebChromeClientListener;

  public JZWebChromeClient()
  {
    this(null);
  }

  public JZWebChromeClient(JZWebListener.WebChromeClientListener webChromeClientListener)
  {
    super();
    this.mWebChromeClientListener = webChromeClientListener;
  }

  @Override
  public void onProgressChanged(WebView view, int newProgress)
  {
    super.onProgressChanged(view, newProgress);
    if (mWebChromeClientListener != null)
      mWebChromeClientListener.onProgressChanged(view, newProgress);
  }

  @Override
  public void onReceivedTitle(WebView view, String title)
  {
    super.onReceivedTitle(view, title);
    if (mWebChromeClientListener != null)
      mWebChromeClientListener.onReceivedTitle(view, title);
  }

  /**
   * 处理定位权限相关问题
   *
   * @author Jack Zhang
   * create at 2019-05-13 11:07
   */
  @Override
  public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissionsCallback geolocationPermissionsCallback)
  {
    super.onGeolocationPermissionsShowPrompt(origin, geolocationPermissionsCallback);
    if (mWebChromeClientListener != null)
      mWebChromeClientListener.onGeolocationPermissionsShowPrompt(origin, geolocationPermissionsCallback);
  }

  @Override
  public boolean onJsAlert(WebView webView, String s, String s1, JsResult jsResult)
  {
    if (mWebChromeClientListener != null)
      return mWebChromeClientListener.onJsAlert(webView, s, s1, jsResult);
    else
      return super.onJsAlert(webView, s, s1, jsResult);
  }

  @Override
  public boolean onJsPrompt(WebView webView, String s, String s1, String s2, JsPromptResult jsPromptResult)
  {
    if (mWebChromeClientListener != null)
      return mWebChromeClientListener.onJsPrompt(webView, s, s1, s2, jsPromptResult);
    else
      return super.onJsPrompt(webView, s, s1, s2, jsPromptResult);
  }

  @Override
  public boolean onJsConfirm(WebView webView, String s, String s1, JsResult jsResult)
  {
    if (mWebChromeClientListener != null)
      return mWebChromeClientListener.onJsConfirm(webView, s, s1, jsResult);
    else
      return super.onJsConfirm(webView, s, s1, jsResult);
  }

  @Override
  public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, FileChooserParams fileChooserParams)
  {
    if (mWebChromeClientListener != null)
      return mWebChromeClientListener.onShowFileChooser(webView, valueCallback, fileChooserParams);
    else
      return super.onShowFileChooser(webView, valueCallback, fileChooserParams);
  }

  @Override
  public void openFileChooser(ValueCallback<Uri> valueCallback, String s, String s1)
  {
    if (mWebChromeClientListener != null)
      mWebChromeClientListener.openFileChooser(valueCallback, s, s1);
    else
      super.openFileChooser(valueCallback, s, s1);
  }
}
