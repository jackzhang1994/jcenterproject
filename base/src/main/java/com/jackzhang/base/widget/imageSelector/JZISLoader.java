package com.jackzhang.base.widget.imageSelector;

import android.content.Context;
import android.widget.ImageView;

import com.jackzhang.base.utils.glide.GlideUtils;
import com.yuyh.library.imgsel.common.ImageLoader;

public class JZISLoader implements ImageLoader
{
  @Override
  public void displayImage(Context context, String path, ImageView imageView)
  {
    GlideUtils.showImage(context, path, imageView);
  }
}
