package com.jackzhang.base.widget.webView.jsBridge;

public interface WebViewJavascriptBridge
{
  public void send(String data);

  public void send(String data, CallBackFunction responseCallback);
}
