package com.jackzhang.base.widget.webView.jsBridge;

public interface CallBackFunction
{
  public void onCallBack(String data);
}
