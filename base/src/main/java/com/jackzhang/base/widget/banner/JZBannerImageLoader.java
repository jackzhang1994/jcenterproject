package com.jackzhang.base.widget.banner;

import android.content.Context;
import android.widget.ImageView;

import com.jackzhang.base.R;
import com.jackzhang.base.utils.glide.GlideUtils;
import com.youth.banner.loader.ImageLoader;

public class JZBannerImageLoader extends ImageLoader
{
  @Override
  public void displayImage(Context context, Object path, ImageView imageView)
  {
    GlideUtils.showImageAndError(context, path, imageView, R.drawable.icon_placeholder_banner);
  }
}
