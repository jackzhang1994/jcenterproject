package com.jackzhang.base.widget.pickerView.view;

import com.bigkoo.pickerview.configure.PickerOptions;
import com.jackzhang.base.utils.ArrayUtils;

import java.util.List;

public class JZSexsPickerView extends JZOptionsPickerView<String>
{
  public JZSexsPickerView(PickerOptions pickerOptions)
  {
    super(pickerOptions);
  }

  public void showSexs()
  {
    showSexs(ArrayUtils.SEXS);
  }

  public void showSexs(List<String> sexsList)
  {
    super.show(sexsList);
  }
}
