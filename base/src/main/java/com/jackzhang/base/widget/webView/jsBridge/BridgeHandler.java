package com.jackzhang.base.widget.webView.jsBridge;

public interface BridgeHandler
{
  void handler(String data, CallBackFunction function);
}
