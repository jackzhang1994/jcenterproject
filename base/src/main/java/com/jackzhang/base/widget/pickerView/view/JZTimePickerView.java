package com.jackzhang.base.widget.pickerView.view;

import com.bigkoo.pickerview.configure.PickerOptions;
import com.bigkoo.pickerview.view.TimePickerView;

public class JZTimePickerView extends TimePickerView
{
  public JZTimePickerView(PickerOptions pickerOptions)
  {
    super(pickerOptions);
  }
}
