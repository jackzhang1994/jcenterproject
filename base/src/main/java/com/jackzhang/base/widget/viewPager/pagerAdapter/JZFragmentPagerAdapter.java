package com.jackzhang.base.widget.viewPager.pagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Fragment ViewPager Adapter
 *
 * @author Jack Zhang
 * create at 2019-04-29 01:09
 */
public class JZFragmentPagerAdapter extends FragmentPagerAdapter
{
  private List<Fragment> mFragments;

  public JZFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragments)
  {
    super(fm);
    mFragments = fragments;
  }

  @Override
  public Fragment getItem(int position)
  {
    return mFragments.get(position);
  }

  @Override
  public int getCount()
  {
    return mFragments.size();
  }
}
