package com.jackzhang.base.widget.viewPager.pagerAdapter;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * 引导页ViewPager Adapter
 *
 * @author Jack Zhang
 * create at 2019-04-29 01:09
 */
public class JZPagerAdapter extends PagerAdapter
{
  private List<View> viewList;

  public JZPagerAdapter(List<View> viewList)
  {
    this.viewList = viewList;
  }

  @Override
  public int getCount()
  {
    return viewList.size();
  }

  @Override
  public boolean isViewFromObject(@NonNull View view, @NonNull Object object)
  {
    return view == object;
  }

  @NonNull
  @Override
  public Object instantiateItem(@NonNull ViewGroup container, int position)
  {
    container.addView(viewList.get(position));
    return viewList.get(position);
  }

  @Override
  public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object)
  {
    container.removeView(viewList.get(position));
  }
}
