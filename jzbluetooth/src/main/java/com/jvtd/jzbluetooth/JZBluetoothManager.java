package com.jvtd.jzbluetooth;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.content.Context;

import com.github.ivbaranov.rxbluetooth.BluetoothConnection;
import com.github.ivbaranov.rxbluetooth.RxBluetooth;

/**
 * 蓝牙管理工具类
 *
 * @author Jack Zhang
 * create at 2019-05-23 15:34
 */
public class JZBluetoothManager
{
  @SuppressLint("StaticFieldLeak")
  private static volatile JZBluetoothManager instance;

  private Context mContext;
  private RxBluetooth mRxBluetooth;
  private BluetoothConnection mBluetoothConnection;

  private JZBluetoothManager(Context context)
  {
    this.mContext = context;
    mRxBluetooth = new RxBluetooth(mContext);
  }

  public static JZBluetoothManager getInstance(Context context)
  {
    if (instance == null)
      synchronized (JZBluetoothManager.class)
      {
        if (instance == null)
          instance = new JZBluetoothManager(context.getApplicationContext());
      }
    return instance;
  }

  public RxBluetooth getRxBluetooth()
  {
    return mRxBluetooth;
  }

  public void initBluetoothConnection(BluetoothSocket bluetoothConnection) throws Exception
  {
    mBluetoothConnection = new BluetoothConnection(bluetoothConnection);
  }

  public BluetoothConnection getBluetoothConnection()
  {
    return mBluetoothConnection;
  }
}
