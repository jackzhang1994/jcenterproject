package com.jackzhang.jcenterproject;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.jackzhang.base.widget.webView.JZWebView;

public class MainActivity extends AppCompatActivity
{

  @TargetApi(Build.VERSION_CODES.M)
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    JZWebView webView = findViewById(R.id.web_view);
    webView.isProgress(true);
    webView.getWebView().isFlexiable(true);//是否具有弹性效果
//        webView.getWebView().loadUrl("http://soft.imtt.qq.com/browser/tes/feedback.html");
//    webView.getWebView().loadUrl("http://debugtbs.qq.com/");
    webView.getWebView().loadUrl("http://m.rd.qinqing88.com/");
//        webView.getWebView().loadUrl("https://www.jianshu.com/p/da757db454ba");

    findViewById(R.id.button).setOnClickListener(v ->
    {
      Intent intent = new Intent(MainActivity.this, SecondActivity.class);
      intent.putExtra("URL", "http://m.rd.qinqing88.com/");
      startActivity(intent);
    });
  }

}
