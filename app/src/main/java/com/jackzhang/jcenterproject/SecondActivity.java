package com.jackzhang.jcenterproject;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

public class SecondActivity extends AppCompatActivity
{
  private Bundle bundle;
  private WebView webView;
  private FrameLayout frameLayout;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_second);

    frameLayout = findViewById(R.id.web_frame);
    bundle = getIntent().getExtras();
    initView();
  }

  private void initView()
  {
    webView = new WebView(this);
    WebSettings settings = webView.getSettings();
    // webview启用javascript支持 用于访问页面中的javascript
    settings.setJavaScriptEnabled(true);

//设置WebView缓存模式 默认断网情况下不缓存
    settings.setCacheMode(WebSettings.LOAD_DEFAULT);


/**
 * LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
 * LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
 * LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
 * LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据。
 */
//断网情况下加载本地缓存
    settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

//让WebView支持DOM storage API
    settings.setDomStorageEnabled(true);

//让WebView支持缩放
    settings.setSupportZoom(true);

//启用WebView内置缩放功能
    settings.setBuiltInZoomControls(true);

//让WebView支持可任意比例缩放
    settings.setUseWideViewPort(true);

//让WebView支持播放插件
    settings.setPluginState(WebSettings.PluginState.ON);

//设置WebView使用内置缩放机制时，是否展现在屏幕缩放控件上
    settings.setDisplayZoomControls(false);

//设置在WebView内部是否允许访问文件
    settings.setAllowFileAccess(true);

//设置WebView的访问UserAgent
//    settings.setUserAgentString("");

//设置脚本是否允许自动打开弹窗
    settings.setJavaScriptCanOpenWindowsAutomatically(true);

// 加快HTML网页加载完成速度
    if (Build.VERSION.SDK_INT >= 19)
      settings.setLoadsImagesAutomatically(true);
    else
      settings.setLoadsImagesAutomatically(false);

// 开启Application H5 Caches 功能
    settings.setAppCacheEnabled(true);

// 设置编码格式
    settings.setDefaultTextEncodingName("utf-8");
    webView.setWebViewClient(new WebViewClient()
    {
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url)
      {
        Log.d("webview", "url: " + url);
        view.loadUrl(url);
        return true;
      }
    });
    frameLayout.addView(webView);
    String url = bundle.getString("URL");
    webView.loadUrl(url);
  }

  //监听BACK按键，有可以返回的页面时返回页面
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event)
  {
    if (keyCode == KeyEvent.KEYCODE_BACK)
    {
      if (webView.canGoBack())
      {
        webView.goBack();
        return true;
      }
    }

    return super.onKeyDown(keyCode, event);
  }

  @Override
  protected void onDestroy()
  {
    if (webView != null)
    {
      webView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
      webView.setTag(null);
      webView.clearHistory();

      ((ViewGroup) webView.getParent()).removeView(webView);
      webView.destroy();
      webView = null;
    }
    super.onDestroy();
  }
}
