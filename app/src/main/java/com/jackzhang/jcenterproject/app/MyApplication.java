package com.jackzhang.jcenterproject.app;

import com.jackzhang.base.app.JZApplication;
import com.jackzhang.base.widget.webView.JZWebViewUtils;

/**
 * Description:
 * Author: Jack Zhang
 * create on: 2019-05-13 14:01
 */
public class MyApplication extends JZApplication
{
  @Override
  public void onCreate()
  {
    super.onCreate();
  }

  @Override
  public void configGlobalInit()
  {
    JZWebViewUtils.initQbSdk(getApplicationContext());
  }
}
